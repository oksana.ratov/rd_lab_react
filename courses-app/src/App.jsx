import React from 'react';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import { mockedAuthorsList, mockedCoursesList } from './constants';

function App() {
	return (
		<>
			<Header />
			<Courses
				coursesList={mockedCoursesList}
				authorsList={mockedAuthorsList}
			/>
		</>
	);
}

export default App;
