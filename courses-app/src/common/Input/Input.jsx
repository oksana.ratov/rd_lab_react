import React from 'react';
import './Input.css';

const Input = (props) => {
	const { labelText, placeholderText, onChange } = props;
	return (
		<input
			className={'input-style'}
			placeholder={placeholderText}
			onChange={onChange}
		/>
	);
};

export default Input;
