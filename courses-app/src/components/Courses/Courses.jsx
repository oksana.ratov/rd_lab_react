import React from 'react';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';

const Courses = ({ authorsList, coursesList }) => {
	return (
		<section className={'section-course'}>
			<SearchBar coursesList={coursesList} />
			{coursesList.map((course) => (
				<CourseCard
					completeAuthorsList={authorsList}
					courses={course}
					key={course.id}
				/>
			))}
		</section>
	);
};

export default Courses;
