import React, { useState } from 'react';
import './SearchBar.css';
import Button from '../../../../common/Button/Button';
import Input from '../../../../common/Input/Input';

const SearchBar = (props) => {
	const { coursesList } = props;
	const [coursesData, setCoursesData] = useState(null);
	const handleSearchInputChange = (e) => {
		const { value } = e.target;
		console.log(value);
		const filteredCourses = coursesList.filter((item) => {
			return (
				item.id.includes(value) || item.title.toLowerCase().includes(value)
			);
		});
		console.log(filteredCourses);
		setCoursesData(filteredCourses);
	};
	console.log('coursesData', coursesData);
	return (
		<div className={'d-flex flex-item d-padding'}>
			<div>
				<Input
					placeholderText={'Enter course name...'}
					onChange={handleSearchInputChange}
				/>
				<Button buttonText='Search' />
			</div>
			<div>
				<Button buttonText='Add new course' />
			</div>
		</div>
	);
};

export default SearchBar;
