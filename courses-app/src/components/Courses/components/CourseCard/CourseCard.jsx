import React from 'react';
import './CourseCard.css';
import Button from '../../../../common/Button/Button';

const CourseCard = ({ completeAuthorsList, courses }) => {
	const { title, description, creationDate, duration, authors } = courses;
	const currentAuthors = completeAuthorsList.filter((item) =>
		authors.includes(item.id)
	);
	const authorsString = currentAuthors.reduce((previousValue, item) => {
		return (previousValue += `${item.name}; `);
	}, '');
	return (
		<div className={'course-item'}>
			<div className={'course-item__body'}>
				<p className={'course-item__title'}>{title}</p>
				<p>{description}</p>
			</div>
			<div className={'course-item__details'}>
				<p className={'course-item__details--bold'}>{duration}</p>
				<p className={'course-item__details--bold'}>{creationDate}</p>
				<p className={'course-item__details--bold'}>{authorsString}</p>
				<div className={'course-item__button'}>
					<Button buttonText='Show course' />
				</div>
			</div>
		</div>
	);
};

export default CourseCard;
