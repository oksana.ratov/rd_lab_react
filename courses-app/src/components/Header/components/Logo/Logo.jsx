import React from 'react';
import './Logo.css';

const Logo = () => {
	return <img className={'logo'} src='/images/logo.png' alt='courses' />;
};

export default Logo;
