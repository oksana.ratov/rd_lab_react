import React from 'react';
import Logo from './components/Logo/Logo';
import './Header.css';
import Button from '../../common/Button/Button';

const Header = () => {
	return (
		<div className={'header d-flex'}>
			<Logo />
			<div className={'d-flex'}>
				<p className={'user'}>Dave</p>
				<Button buttonText='Logout' />
			</div>
		</div>
	);
};

export default Header;
